import requests
import json
import random
from flask import Flask
from flask import request

app = Flask(__name__)

SERVICE_URL= 'https://api-gateway.yalochat.com/jazz/position/qa'
CALL_BACK = 'http://1798e83e.ngrok.io/'
API_KEY = 'jsT6jmN2y173R9yzNaboR11fb1XRFQ406PG5p721'
USER = str(random.randint(100000000, 999999999))
STATES = ['start', 'options', 'search']
REQUEST_PER_STATE = {
    'start': 3,
    'options': 1,
    'search': 1
}
RESPONSES = list()

CURRENT_STATE = None

TEST = {
    "user": "502456789123",
    "start": "restart",
    "option": "3",
    "search": "Quiero un vuelo para 2 a Ciudad de Mexico",
    "flight": {
        "roundtrip": "Si",
        "departure": "Ciudad de México",
        "arrival": "Toronto",
        "passangers": 2,
        "departure-date": "20-12-2019",
        "return-date" : "01-01-2020"
    }
}
STATE_MAP_VERBS = {
    'start': 'start',
    'options': 'options',
    'search': 'search'
}


@app.route('/', methods=['POST'])
def response_from_server():
    data = json.loads(request.data)
    print(data)
    RESPONSES.append(data)
    return request.data


def make_request(text):
    payload = {
        "user": USER,
        "text": text,
        "callback": CALL_BACK
    }
    print(f'make request')
    headers = {'x-api-key': API_KEY, 'Content-Type': 'application/json'}
    r = requests.post(SERVICE_URL, data=json.dumps(payload), headers=headers)
    return r


def make_conversation():
    states = { 
     'start': 'options',
     'options': 'search'
    }
    global CURRENT_STATE
    global RESPONSES
    global REQUEST_PER_STATE

    if not CURRENT_STATE:
        CURRENT_STATE = STATE_MAP_VERBS['start']
        response = make_request(CURRENT_STATE)
        return 'start'
    elif CURRENT_STATE == 'options':
        response = make_request(TEST['option'])

        print(RESPONSES[-1])
        return 'go to search'
    elif CURRENT_STATE == 'search':
        response = make_request(TEST['search'])
        print(json.dump())
        return 'search'
    else:
        prev_state = CURRENT_STATE
        CURRENT_STATE = states[CURRENT_STATE]
        if REQUEST_PER_STATE[CURRENT_STATE] != len(RESPONSES):
            print(f'current state: {CURRENT_STATE} number of responses {len(RESPONSES)}')
            print('not ready to pass next state')
            CURRENT_STATE = prev_state
            return 'waiting for server response'
        else:
            print(f'go to {CURRENT_STATE}')
            RESPONSES = []
            response = make_request(CURRENT_STATE)
            return f'got to {CURRENT_STATE}'
        




@app.route('/start', methods=['GET'])
def start():
    response = make_conversation()
    return response






